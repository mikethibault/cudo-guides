#!/bin/bash
#This script is intended to allow users to add the standard CudoOS features to any ubuntu 20.4 or later based linux distro.
#Preferrably Xubuntu
nvidia=0
tempbool=0
GAMING=0
amd=0
cudobeta=0
composition=0

echo ""#newline
echo ""#newline
echo "Before we begin, please note the following:"
echo "This update requires downloading several gigabytes of data, it is not recommended to run this over a metered connection."
echo "The install will take upwards of an additional 8gb of storage space and requires persistant storage so it will be available after a reboot"
echo "To add emphasis, a reboot will be required upon completion, this is for DKMS graphics drivers to take effect."
echo "Additionally this install will require superuser (admin) privilages, and thus may not work on virtual/cloud machines or company machines"
echo "Continue?"

while [ "$tempbool" = 0 ]
do
  read yn
  if [ $yn = "yes" ] || [ $yn = "Yes" ] || [ $yn = "YES" ] || [ $yn = "y" ] || [ $yn = "Y" ]
  then
   tempbool=1
   break
  elif [ $yn = "no" ] || [ $yn = "No" ] || [ $yn = "NO" ] || [ $yn = "n" ] || [ $yn = "N" ]
  then
   exit
   break
  else
   echo "Please answer yes or no."
  fi
done

echo ""#newline
echo ""#newline
echo "Are you using any AMD cards?"
echo "Please answer yes or no."

while [ "$amd" = 0 ]
do
  read yn
  if [ $yn = "yes" ] || [ $yn = "Yes" ] || [ $yn = "YES" ] || [ $yn = "y" ] || [ $yn = "Y" ]
  then
   amd=2
   break
  elif [ $yn = "no" ] || [ $yn = "No" ] || [ $yn = "NO" ] || [ $yn = "n" ] || [ $yn = "N" ]
  then
   amd=-1
   break
  else
   echo "Please answer yes or no."
  fi
done


if [ "$amd" = 2]
then
  echo ""#newline
  echo ""#newline
  echo "AMD devices currently require kernel version 4.5 to work correctly"
  echo "This install will attempt to install kernel 4.5 on the system, this should become default on next boot."
  echo "if it does not, select Advanced Options from the boot menu and choose to boot with kernel 4.5"
  echo ""#newline
  echo "WARNING: kernel 4.5 is over 4 years old and has several known security flaws."
  echo "   While viruses in Linux are very rare, for security and liability reasons, it's advised not to keep any personal data on the machine with this in place."
  echo "WARNING: Kernel 4.5 does NOT work with PoP OS! It is advised to use any other Debian or Ubuntu based distro."
  echo "WARNING: AMD support has not been fully tested, you can choose 'no' and manually install drivers and kernel yourself"
  echo "Are you sure you wish to install the AMD GPU drivers?"
  while [ "$amd" = 2 ]
  do
    read yn
    if [ $yn = "yes" ] || [ $yn = "Yes" ] || [ $yn = "YES" ] || [ $yn = "y" ] || [ $yn = "Y" ]
    then
     amd=1
     break
    elif [ $yn = "no" ] || [ $yn = "No" ] || [ $yn = "NO" ] || [ $yn = "n" ] || [ $yn = "N" ]
    then
     exit
     break
    else
     echo "Please answer yes or no."
    fi
done

echo ""#newline
echo ""#newline
echo "Are you using any Nvidia cards?"
echo "NOTE: you can choose 'no' and manually install drivers yourself, manually installed drivers may not work correctly."
echo "Please answer yes or no."

while [ "$nvidia" = 0 ]
do
  read yn
  if [ $yn = "yes" ] || [ $yn = "Yes" ] || [ $yn = "YES" ] || [ $yn = "y" ] || [ $yn = "Y" ]
  then
   nvidia=1
   break
  elif [ $yn = "no" ] || [ $yn = "No" ] || [ $yn = "NO" ] || [ $yn = "n" ] || [ $yn = "N" ]
  then
   nvidia=-1
   break
  else
   echo "Please answer yes or no."
  fi
done


#steam NEEDS the i386 architecture, because they never moved to 64-bit, even though they stopped supporting x86
echo ""#newline
echo ""#newline
echo "Do you intend to install Steam on this device?"
echo "This will take additional space to install the i386 drivers required by steam"
echo "This can NOT be re-done later"
echo "please enter yes or no"
while [ "$GAMING" = 0 ]
do
  read yn
  if [ $yn = "yes" ] || [ $yn = "Yes" ] || [ $yn = "YES" ] || [ $yn = "y" ] || [ $yn = "Y" ]
  then
   sudo dpkg --add-architecture i386
   sudo apt-get update
   GAMING=1
   break
  elif [ $yn = "no" ] || [ $yn = "No" ] || [ $yn = "NO" ] || [ $yn = "n" ] || [ $yn = "N" ]
  then
   GAMING=1
   break
  else
   echo "Please answer yes or no."
  fi
done

echo ""#newline
echo ""#newline
echo "Would you like to disable window composition?"
echo "This improves GPU performance by taking the UI rendering workload off the GPU"
echo "This may disable some graphical effects in some window managers"
echo "NOTE: This currently only supports XFCE and KDE. There is partial support for GNOME and Budgie, for these the composition workload is reduced but not removed."
echo "This will not effect any other window managers."
echo "Please answer yes or no."

while [ "$composition" = 0 ]
do
  read yn
  if [ $yn = "yes" ] || [ $yn = "Yes" ] || [ $yn = "YES" ] || [ $yn = "y" ] || [ $yn = "Y" ]
  then
   composition=1
   break
  elif [ $yn = "no" ] || [ $yn = "No" ] || [ $yn = "NO" ] || [ $yn = "n" ] || [ $yn = "N" ]
  then
   composition=-1
   break
  else
   echo "Please answer yes or no."
  fi
done

echo ""#newline
echo ""#newline
echo "Would you like to update Cudo from the stable builds?"
echo "If no, the experimental builds will be used, these builds have latest features but they can be unstable."
echo "please enter yes or no."
while [ "$cudobeta" = 0 ]
do
  read yn
  if [ $yn = "yes" ] || [ $yn = "Yes" ] || [ $yn = "YES" ] || [ $yn = "y" ] || [ $yn = "Y" ]
  then
   cudobeta=1
   break
  elif [ $yn = "no" ] || [ $yn = "No" ] || [ $yn = "NO" ] || [ $yn = "n" ] || [ $yn = "N" ]
  then
   cudobeta=-1
   break
  else
   echo "Please answer yes or no."
  fi
done

echo "" #newline
echo "" #newline
echo "Please enter your cudo organization."
echo "This can be found on the web dashboard under 'Settings > Organization' as the 'Username' field"
echo "You can also follow this link: https://console.cudominer.com/settings/organization "
read org
tempbool=0
while [ "$tempbool" = 0 ]
do
  echo "" #newline
  echo "you have entered '$org' is this correct? Please enter yes or no."
  read yn
  if [ $yn = "yes" ] || [ $yn = "Yes" ] || [ $yn = "YES" ] || [ $yn = "y" ] || [ $yn = "Y" ]
  then
   sudo echo "org=$org" > /etc/cudo_minerrc
   tempbool=1
   break
  elif [ $yn = "no" ] || [ $yn = "No" ] || [ $yn = "NO" ] || [ $yn = "n" ] || [ $yn = "N" ]
  then
   echo "please re-enter your organization name"
   read org
  else
   echo "Please answer yes or no."
  fi
done


sudo apt-get -q update


#install AMD kernel and drivers
if [ "$amd" = 1]
then
  echo ""#newline
  echo ""#newline
  echo "Installing Kernel 4.5 and AMD drivers"
  sudo apt-get install linux-image-5.4.0-42-generic linux-modules-5.4.0-42-generic linux-modules-extra-5.4.0-42-generic linux-headers-5.4.0-42-generic
  sudo apt-mark hold linux-generic-hwe-20.04 linux-image-generic-hwe-20.04 linux-headers-generic-hwe-20.04
  sudo wget -O https://drivers.amd.com/drivers/linux/amdgpu-pro-20.45-1188099-ubuntu-20.04.tar.xz
  sudo mkdir ./amdtemp
  sudo tar -Jxvf ./amdgpu-pro-20.45-1188099-ubuntu-20.04.tar.xz ./amdtemp
  sudo ./amdtemp/amdgpu-pro-install -y
  
  echo ""#newline
  echo ""#newline
  echo "cleaning up"
  sudo rm ./amdgpu-pro-20.45-1188099-ubuntu-20.04.tar.xz
  sudo apt-get clean

fi



#install nvidia drivers
if [ "$nvidia" = 1]
then
  echo ""#newline
  echo ""#newline
  echo "Installing Nvidia Drivers and the CUDA Toolkit"
  sudo apt-get -q -y install nvidia-driver-470

  #also part of nvidia, download and install the CUDA-Toolkit, because T-Rex needs that. This must be done AFTER 460, because steam.
  wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
  sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
  sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/7fa2af80.pub
  sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /"
  sudo apt-get -q update
  sudo apt-get -y -q install cuda
  echo ""#newline
  echo ""#newline
  echo "cleaning up and adding support for overclocking"
  sudo apt-get clean

  #enable overclocking support
  sudo nvidia-xconfig -a --allow-empty-initial-configuration --cool-bits=31
fi

#Basic cleanup.
sudo apt-get -q -y autoremove && sudo apt-get clean


#GPU compositor bad, turn it off.
if [ "$composition" = 1 ]
then 
#cover XFCE
  if dpkg-query -W --showformat='${Status}\n' xfconf | grep -q "ok installed" 
  then
    echo "XFCE found, disabling composition"
    xfconf-query -c xfwm4 -p /general/use_compositing -s false
  fi
  #cover KDE
  if dpkg-query -W --showformat='${Status}\n' qdbus | grep -q "ok installed" 
  then
    echo "KDE found, disabling composition"
    qdbus org.kde.KWin /Compositor suspend
  fi
  #cover GNOME and budgie. NOTE: this only disables vsync, mutter does not allow for full composition disabling, afik this is the best we get.
  if dpkg-query -W --showformat='${Status}\n' mutter | grep -q "ok installed" 
  then
   FILE=~/.profile
   FOLDER=~/.profile.d
   if [ test -f "$FILE" ]
   then
    echo "Mutter found, disabling composition vsync"
    echo "export CLUTTER_VBLANK=none" >> ~/.profile
   elif [  test -f "$FOLDER"  ]
   then
    echo "Mutter found, disabling composition vsync"
    echo "export CLUTTER_VBLANK=none" >> ~/.profile.d/mutter-no-vsync.sh
    chmod +rx ~/.profile.d/mutter-no-vsync.sh
   fi
  fi
#todo: cover other compositors, primarily gnome 2 (cinnimon/linux mint), enlightenment, LXDE (Openbox?).

fi



#Install cudo GUI
echo ""#newline
echo ""#newline
if [ "$cudobeta" = -1 ]
then
 echo "Installing the stable Cudo Miner APT repository..."
 sudo echo -n 'deb [arch=amd64] https://download.cudo.org/repo/apt/ stable main' > /etc/apt/sources.list.d/cudo.list
 break
elif [ "$cudobeta" = 1 ]
then
 echo "Installing the experimental Cudo Miner APT repository..."
 sudo echo -n 'deb [arch=amd64] https://download.cudo.org/repo/apt/ experimental main' > /etc/apt/sources.list.d/cudo.list
 break
fi

sudo wget -O https://download.cudo.org/keys/pgp/apt.asc
sudo mv ./apt.asc /etc/apt/trusted.gpg.d/cudo.asc
sudo apt-get update
sudo apt-get -q install cudo-miner-desktop cudo-miner-headless

echo "" #newline
echo "" #newline
echo "" #newline
echo "" #newline
cudominercli
echo "" #newline
echo "" #newline
echo "Congradulations! The installation is complete, you can now find Cudo Miner in your applications."
echo "You may also control it using the cudominercli command."
echo "Additionally you may administrate this device from the web console at https://console.cudominer.com/devices/ "
echo "" #newline
echo "Please remember the GUI and CLI tools are only to control configuration, closing them does NOT end mining, clicking 'Disable' or running 'cudominercli disable' ends mining"

echo "" #newline
echo "" #newline

echo "for the install to finish, please restart the device."

echo "" #newline
echo "" #newline
