# Cudo Ambassador Guides

NOTICE: Remember that cudo staff and ambassadors will never ask for your password or for payment of services.


<hr/>
<details><summary>Troubleshooting</summary><br/>
<details><summary>Miner not working/All jobs failing</summary>
<br/>

- Be sure antivirus is setup correctly, including windows defender.
<br/>Disabling the antivirus, and/or adding exceptions for individual files <b>will not work.</b>
<br/>The registry hacks for disabling windows defender <b>do not work.</b>
<br/>Our guides to setting up each antivirus are listed here: <br/>[https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/antivirus.md](https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/antivirus.md)

- If you are running AMD and Nvidia cards in the same machine, you have to define what cards each miner uses, the detailed instructions for this are here:<br/>[https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/multiarch.md](https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/multiarch.md)

- If your GPU is under 5gb, then you can only have ETC and BTG miners used on that card, sometimes RVN works.

- If it's under 4gb then you can only have BTG enabled.

- IMPORTANT: If you are mining ETC, BTG, or RVN, be sure to enable auto-convert on the web dashboard under `settings > Payment`, since none of those can be withdrawn currently.<br/> You can also get to the auto-convert page by following this link: [https://console.cudominer.com/settings/payment](https://console.cudominer.com/settings/payment)

- If the card is 6gb or more, try setting the phoenix version from automatic to 5.5b, under advanced GPU settings on the main tab.

- To use the T-Rex miner on Nvidia cards you are required to use version 10.0 or 11.1 of the CUDA toolkit, which are available from the following link. NOTE: These versions are outdated and may not work as expected with 30 series cards.<br/>
[https://developer.nvidia.com/cuda-toolkit-archive](https://developer.nvidia.com/cuda-toolkit-archive)

- Some cards require overclocking enabled in the settings.

- You can try creating a new config for the organization, to be sure it's not a settings related issue
[https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/qeue.md](https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/qeue.md)

- In some extreme cases it may be due to driver error, when drivers update sometimes they leave behind invalid files from old versions, this is especially common when downgrading drivers. We have some more advanced guides on handling this stuff here:<br/>
[https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/ddu.md](https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/ddu.md)
</details>
</details>
<br/><hr/>
<details><summary>FAQ</summary>
<br/>
<details><summary>Why does the earnings fluctuate so much?</summary>
<br/>
The earnings are primarily based on three things:<br/>
- The transactions processed, which there's no real way to tell this aside from market trade volume, higher trade volume, more chances to process transactions and take a cut of the GAS fees.<br/>
- Shares accepted, this can also be random, they become harder to get as difficulty goes up, and you get more chances to find them based on the hashrate you have.<br/>
- Currency Value, this one really only applies if you're using Auto-convert from one currency to another, but as the values for those currencies go up and down, so do the amounts you get.<br/>
</details>
</details>
<br/><hr/>
<details><summary>Pool mining with Cudo</summary>
To do pool mining with Cudo you will need a 3rd party miner, you can bring your own, but here's a list of official links to commonly used miners.<br/><br/>
- Phoenix (GPU only): [https://bitcointalk.org/index.php?topic=2647654.msg56968316#msg56968316](https://bitcointalk.org/index.php?topic=2647654.msg56968316#msg56968316)
<br/><br/>
- T-Rex (Nvidia only, requires CUDA toolkit):<br/>
Linux: [http://trex-miner.com/download/t-rex-0.20.3-linux.tar.gz](http://trex-miner.com/download/t-rex-0.20.3-linux.tar.gz)<br/>
Windows: [http://trex-miner.com/download/t-rex-0.20.3-win.zip](http://trex-miner.com/download/t-rex-0.20.3-win.zip)<br/>
Please note that T-Rex requires the CUDA Toolkit drivers, which are not shipped with standard or game ready drivers. These optional drivers can be obtained here: <br/>
[https://developer.nvidia.com/cuda-downloads](https://developer.nvidia.com/cuda-downloads)
<br/><br/>
- Team Red Miner (AMD GPU only): [https://github.com/todxx/teamredminer/releases](https://github.com/todxx/teamredminer/releases)
<br/><br/>
- XMRig (Mainly aimed at CPU): [https://xmrig.com/download](https://xmrig.com/download)

<br/><br/>
After you have the pool miner of your choice, you will need the pool info for the command line, this can be found on the web dashboard under `Management > Devices > Connect A Device > ASIC`<br/> Or by following this link and choosing ASIC. [https://console.cudominer.com/devices/setup](https://console.cudominer.com/devices/setup)
</details>

